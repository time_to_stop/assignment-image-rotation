#include "bmp.h"

#include "../image/pixel.h"

#define BMP_SIGNATURE 0x4D42
#define BMP_BIT_COUNT 24
#define BMP_SIZE 40
#define BMP_PLANES 1
#define BMP_RESERVED_ZERO 0
#define ALIGN 3

const char* read_errors[] = {
    [READ_OK]="BMP was read successfully",
    [READ_INVALID_SIGNATURE]="BMP invalid signature",
    [READ_INVALID_IMAGE_META]="BMP invalid image meta",
    [READ_INVALID_HEADER]="BMP invalid header",
    [READ_FAILED_TO_ALLOCATE_MEMORY]="BMP failed to allocate memory for image",
    [READ_INVALID_BITS]="BMP invalid image source"
};

const char* write_errors[] = {
    [WRITE_OK]="BMP was written successfully",
    [WRITE_ERROR]="BMP error occurred while writing"
};

enum read_status from_bmp(FILE* in, struct image* img)
{
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
    {
        return READ_INVALID_HEADER;
    }

    if (header.bfType != BMP_SIGNATURE)
    {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != BMP_BIT_COUNT || header.biPlanes != BMP_PLANES)
    {
        return READ_INVALID_BITS;
    }

    if (!create_image(img, header.biWidth, header.biHeight))
    {
        return READ_FAILED_TO_ALLOCATE_MEMORY;
    }

    for (uint64_t i = 0; i != header.biHeight; i++)
    {
        if (fread(get_pixel(img, 0, i), sizeof(struct pixel), header.biWidth, in) != header.biWidth)
        {
            return READ_INVALID_BITS;
        }

        if (header.biWidth % 4 != 0 && fseek(in, (4 - (int32_t)((header.biWidth * sizeof(struct pixel)) % 4)), SEEK_CUR) != 0)
        {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, const struct image* img)
{
    char empty[ALIGN] = { 0 };
    struct bmp_header header;

    uint64_t adjust_width = img->width + (img->width % 4 == 0 ? 0 : 4 - (img->width * sizeof(struct pixel)) % 4);

    header.bfType = BMP_SIGNATURE;
    header.bFileSize = sizeof(struct bmp_header) + img->height * adjust_width * sizeof(struct pixel);
    header.bfReserved = BMP_RESERVED_ZERO;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BMP_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = BMP_PLANES;
    header.biBitCount = BMP_BIT_COUNT;
    header.biCompression = BMP_RESERVED_ZERO;
    header.biSizeImage = img->height * adjust_width * sizeof(struct pixel);
    header.biXPelsPerMeter = BMP_RESERVED_ZERO;
    header.biYPelsPerMeter = BMP_RESERVED_ZERO;
    header.biClrUsed = BMP_RESERVED_ZERO;
    header.biClrImportant = BMP_RESERVED_ZERO;

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
    {
        return WRITE_ERROR;
    }

    for (uint64_t i = 0; i != img->height; i++)
    {
        if (fwrite(get_pixel(img, 0, i), sizeof(struct pixel), img->width, out) != img->width)
        {
            return WRITE_ERROR;
        }

        if (img->width % 4 != 0 && fwrite(empty, sizeof(char), 4 - (img->width * sizeof(struct pixel) % 4), out) != 4 - (img->width * sizeof(struct pixel) % 4))
        {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}

const char* read_error(enum read_status status)
{
    return read_errors[status];
}

const char* write_error(enum write_status status)
{
    return write_errors[status];
}

