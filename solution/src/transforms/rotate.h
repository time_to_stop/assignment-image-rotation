#ifndef IMAGE_ROTATION_ROTATE_H
#define IMAGE_ROTATION_ROTATE_H

#include "../image/image.h"

bool rotate(struct image* source, struct image* destination);

#endif //IMAGE_ROTATION_ROTATE_H
