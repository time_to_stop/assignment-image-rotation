#include "rotate.h"
#include "../image/pixel.h"

bool rotate(struct image* source, struct image* destination)
{
    if (!create_image(destination, source->height, source->width))
    {
        return false;
    }

    for (uint32_t i = 0; i < source->height; i++)
    {
        for (uint32_t j = 0; j < source->width; j++)
        {
            struct pixel* p =  get_pixel(source, j, i);

            set_pixel(destination, source->height - 1 - i, j, p);
        }
    }

    return true;
}

