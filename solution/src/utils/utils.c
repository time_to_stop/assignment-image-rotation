#include "utils.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

noreturn void error(const char* msg, ...)
{
    va_list args;
    va_start (args, msg);
    vfprintf(stderr, msg, args);  // NOLINT
    va_end (args);
    exit(EXIT_FAILURE);
}
