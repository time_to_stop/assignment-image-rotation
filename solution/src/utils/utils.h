#ifndef IMAGE_ROTATION_UTILS_H
#define IMAGE_ROTATION_UTILS_H

#include <stdbool.h>
#include <stdnoreturn.h>

noreturn void error(const char* msg, ...);

#endif //IMAGE_ROTATION_UTILS_H
