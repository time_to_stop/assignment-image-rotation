#include <stdio.h>
#include <stdlib.h>

#include "bmp/bmp.h"
#include "transforms/rotate.h"
#include "utils/utils.h"

int main(int argc, char* argv[])
{
    if (argc != 3) error("Usage: %s <source-input-file> <transformed-output-file>\n", argv[0]);

    FILE* input = fopen(argv[1], "rb");

    if (input == NULL) error("Unable to open file: %s\n", argv[1]);

    FILE* output = fopen(argv[2], "wb");

    if (output == NULL)
    {
        fclose(output);
        error("Unable to open file: %s\n", argv[2]);
    }

    struct image source, rotated;

    enum read_status read_code = from_bmp(input, &source);

    if (read_code != READ_OK)
    {
        destroy_image(&source);
        fclose(input);
        fclose(output);
        error("Read BMP failed:\n%s\n", read_error(read_code));
    }

    if (rotate(&source, &rotated) != true)
    {
        destroy_image(&source);
        destroy_image(&rotated);
        fclose(input);
        fclose(output);
        error("Failed to allocate memory for image\n");
    }

    enum write_status write_code = to_bmp(output, &rotated);

    if (write_code != WRITE_OK)
    {
        destroy_image(&source);
        destroy_image(&rotated);
        fclose(input);
        fclose(output);
        error("Write BMP failed:\n%s\n", write_error(write_code));
    }

    destroy_image(&source);
    destroy_image(&rotated);
    fclose(input);
    fclose(output);

    return EXIT_SUCCESS;
}
