#ifndef IMAGE_ROTATION_PIXEL_H
#define IMAGE_ROTATION_PIXEL_H

#include <stdint.h>

struct pixel
{
    uint8_t b, g, r;
};

#endif //IMAGE_ROTATION_PIXEL_H
